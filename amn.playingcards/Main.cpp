#include <iostream>
#include <conio.h>

enum Rank
{
    Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
    Spades, Hearts, Diamonds, Clubs
};

struct Card
{
    Rank rank;
    Suit suit;
};

int main()
{

    _getch();
    return 0;
}